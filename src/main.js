// stylesheets
import "reset-css";
import "./style/app.sass";
import "prismjs/themes/prism-tomorrow.css";
// dependencies
import Vue from "vue";
import Prism from "prismjs/components/prism-core.min.js";
import "prismjs/components/prism-yaml.min.js";
import "prismjs/plugins/keep-markup/prism-keep-markup.min.js";
import LZString from "lz-string";

// initialize prism.js
Prism.highlightAll();

// initialize vue.js
window.Vue = Vue;
const app = new Vue({
    el: "main",
    data: {
        input: {
            method: "",
            reverseProxy: true,
            publicPath: ""
        },
        config: {
            valid: true,
            domain: "",
            protocolSSL: false,
            addPort: false,
            subpath: "",
            port: 3000
        }
    },
    methods: {
        updateConfigURL() {
            try {
                let url = new URL(this.input.publicPath);
                this.config.valid = true;
                this.config.domain = url.hostname;
                this.config.protocolSSL = url.protocol === "https:";
                this.config.subpath = url.pathname.substr(1).replace(/\/$/, "");
                if (this.input.reverseProxy) {
                    this.config.port = url.port === "" ? 3000 : parseInt(url.port);
                } else {
                    if (url.port === "") {
                        this.config.port = this.config.protocolSSL ? "443" : "80";
                    } else {
                        this.config.port = parseInt(url.port);
                    }
                }
                this.config.addPort = url.port !== "";
            } catch (e) {
                this.config.valid = false;
            }
        },
        downloadConf() {
            const dlLink = document.createElement("a");
            const format = this.input.method === "docker" ? "yml" : "json";
            dlLink.classList.add("hidden");
            dlLink.download = this.input.method === "docker" ? "docker-compose.yml" : "config.json";
            dlLink.href = "data:text/" + format + ";base64," + btoa(
                document.querySelector("pre.language-" + format).innerText.trim()
            );
            document.body.appendChild(dlLink);
            dlLink.click();
            setTimeout(() => dlLink.parentElement.removeChild(dlLink), 3500);
        },
        createShareURL() {
            const compressed = LZString.compressToEncodedURIComponent(JSON.stringify(this.input));
            window.location.hash = "#c=" + compressed;
            const shareBtn = document.querySelector(".share");
            let cacheMsg = shareBtn.innerText;
            shareBtn.innerText = "URL of this page updated ...";
            setTimeout(() => shareBtn.innerText = cacheMsg, 5000);
        }
    }
});

if (window.location.hash.indexOf("#c=") === 0) {
    let data = LZString.decompressFromEncodedURIComponent(window.location.hash.substr(3));
    data = JSON.parse(data);
    if (data.hasOwnProperty("method") && data.length === app.input.length) {
        app.input = data;
        app.updateConfigURL();
    }
}
