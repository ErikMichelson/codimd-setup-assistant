const path = require("path");
const webpackHtmlPlugin = require("html-webpack-plugin");
const webpackCleanPlugin = require("clean-webpack-plugin");
const webpackMiniCssExtractLoader = require("mini-css-extract-plugin");
const webpackOptimizeCSSPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
    entry: {
        app: "./src/main.js"
    },
    output: {
        path: path.resolve('dist'),
        filename: "[name].[contenthash].js"
    },
    plugins: [
        new webpackHtmlPlugin({
            template: "./src/index.ejs"
        }),
        new webpackCleanPlugin.CleanWebpackPlugin(),
        new webpackMiniCssExtractLoader({
            filename: "[name].[contenthash].css"
        }),
        new webpackOptimizeCSSPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    webpackMiniCssExtractLoader.loader,
                    "css-loader"
                ]
            },
            {
                test: /\.sass$/,
                use: [
                    webpackMiniCssExtractLoader.loader,
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    "file-loader"
                ]
            }
        ]
    },
    resolve: {
        alias: {
            "vue$": "vue/dist/vue.esm.js"
        }
    }
};
