# CodiMD setup assistant

This web tool is made to assist you while installing your own instance of [CodiMD](https://codimd.org).  
You will find the latest version of this tool on GitLab pages [here](https://erikmichelson.gitlab.io/codimd-setup-assistant/).


## Build for yourself
1. Install nodeJS (with npm)
2. Clone this repo
3. Run `npm install`
4. Run `npm run build`
5. You will find the built files in the `/dist` directory. Just open up the `index.html`.

## Development
Same procedure as with normal build but instead of `npm run build` you might want to run `npm run dev`.
For detailed debugging the [Vue-DevTools extension](https://github.com/vuejs/vue-devtools) is recommended. 
